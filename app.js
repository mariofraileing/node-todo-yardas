const argv = require('./config/yargs').argv;
const colors = require('colors');

const todo = require('./todo');

let newCommand = argv._[0];

switch (newCommand) {
    case 'crear':
        let task = todo.create(argv.description);
        console.log(task);
        break;
    case 'listar':
        let todoList = todo.getDataList();
        for (let task of todoList) {
            console.log('=======Tarea por hacer==========='.gray);
            console.log(task.description);
            console.log('Estatus: ', task.completed);
            console.log('================================='.gray);

        }
        break;
    case 'actualizar':
        let updated = todo.updateTask(argv.description, argv.completed);
        console.log(updated);
        break;
    case 'borrar':
        let deleteTask = todo.delteTask(argv.description);
        console.log(deleteTask);
        break;
    default:
        console.log('comando no reconocido');

}