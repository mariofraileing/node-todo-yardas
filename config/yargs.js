const description = {
    demand: true,
    alias: 'd',
    desc: 'Descripcion de la tarea por hacer'
};

const completed = {
    demand: true,
    alias: 'c',
    desc: 'Marca como completado o pendiente la tarea.'
};

const argv = require('yargs')
    .command('crear', 'crear tarea por hacer', {
        description,
    })
    .command('actualizar', 'actualizar las tareas por hacer', {
        description,
        completed
    })
    .command('borrar', 'borra una tarea', {
        description,
    })
    .help()
    .argv;


module.exports = {
    argv
};