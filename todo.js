const fs = require('fs');
let todoList = [];

const saveToDb = () => {
    let data = JSON.stringify(todoList);
    fs.writeFile(`db/data.json`, data, (err) => {
        if (err) throw new Error('No se pudo grabar', err);

    });
};

const getDbData = () => {

    try {
        todoList = require('../04-todo/db/data.json');
    } catch (error) {
        todoList = [];
    }


};


const create = (description) => {

    getDbData();
    let todo = {
        description,
        completed: false
    };

    todoList.push(todo);
    saveToDb();
    return todo;
}

const getDataList = () => {
    getDbData();
    return todoList;
}

const updateTask = (description, completed = true) => {
    getDbData();
    let index = todoList.findIndex(task => task.description === description);
    if (index >= 0) {
        todoList[index].completed = completed;
        saveToDb();
        return true;
    } else {
        return false;
    }
}

const delteTask = (description) => {
    getDbData();
    let newTodoList = todoList.filter(task => {

        return task.description !== description;

    });

    if (todoList.length === newTodoList.length) {
        return false;
    } else {
        todoList = newTodoList;
        saveToDb();
        return true;
    }


};

module.exports = {
    create,
    getDataList,
    updateTask,
    delteTask
}